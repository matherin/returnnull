﻿using System;
using System.Text;
using System.Collections.Generic;

using Moq;
using System.Web;
using System.Web.Routing;
using NUnit.Framework;
using System.Web.Mvc;

namespace EachVoice.Tests
{
    /// <summary>
    /// -zm testing routes
    /// </summary>
    [TestFixture]
    public class RoutesTest : RoutesBaseTest
    {
        //public TestRoutesBase()
        [SetUp]
        public void SetUp()
        {
            routes = RouteTable.Routes;
            EachVoice.RouteConfig.RegisterRoutes(routes);

            //RouteAssert.UseAssertEngine(new NunitAssertEngine());
        }
        
        [Test]
        public void DefaultURL_ShouldMapTo_Home_Index()
        {
            TestRouteMatch("~/", "Home", "Index");
        }

        [Test,Description("~/Home/About should has controller Home and action method about")]
        public void Home_About()
        {
            TestRouteMatch("~/Home/About","Home","About");
        }

        [Test, Description("~/Home/Edit with additioanl segment of route value")]
        public void Edit_GET()
        {
            TestRouteMatch("~/Home/Edit/{blid}/{uid}", "Home", "Edit",new { blid= "bill_Id", uid= "uid" },"Get");
        }
        [Test,Description("the url does not match the place holder which predefined at route config should fail")]
        public void Fail_WrongUrl( )
        {
            TestRouteFail("~/Home/Index/Y/Z");
        }
        //public void Dispose() clean up for future routes testing
        [TearDown]
        public void TearDown()
        {
            RouteTable.Routes.Clear();
        }

       
    }
}
