﻿$(document).ready(function () {
    var str = "";
    var actionStr = "";
    var sourceStr = "";
    var voteStr = "";
    var tblstr = "";
    var tr = "";
    $('#searchkey').click(function (e)
    {       
        e.preventDefault();
        var key = $('#key').val();
        var state = "wa";
        var ttl = "";
        var bill_id = "";
        $('#fromTag').html('');
        $('#fromTag').addClass('jumbotron');
        $.getJSON("https://openstates.org/api/v1/bills/?state="+state+"&subject="+key, function (data) {
            $.each(data,
                function () {
                    
                    $('#fromTag').append("<a class='url' href='#' id=\"" + this.id +"\" data-title=\""+ this.title +"\">" + this.title + "</a> <br /><br />");                    
                });
            $('.url').click( function (e) {
                e.preventDefault();
                bill_id = $(this).attr('id');
                ttl = $(this).attr('data-title');
                console.log(ttl);
                console.log(bill_id);
                //this is what i need(; 
                $(document).click(function () { $('#fromTag').html(''); $('#fromTag').removeClass('jumbotron'); });
                $.getJSON("https://openstates.org/api/v1/bills/" + bill_id+"/", function (data) {
                    //console.log(data);
                    //console.log(data.actions);
                    //console.log(data.votes[0]);
                    $('#fromTag').html('');
                    str += "<div class='text-center'><a href='/Home/CreateComments/?bill_id=" + bill_id +"&ttl="+ttl+"'"+"class='text-danger h3'>Join the discussion</a></div><br /><br /><br />";

                    str += "<h3>Session:&nbsp" + data.session + "&nbsp&nbsp&nbsp&nbsp Id:&nbsp" + data.id + "</h3><br />";

                    sourceStr += "<h3>Sources: </h3><br />";
                    $.each(data.sources, function (i, item) {
                        sourceStr += "<a target=_'blank' href='" + item.url + "'>" + item.url + "</a><br />"
                        
                    });
                    //str += sourceStr;

                    voteStr += "<h3>Votes: </h3><br />";
                    $.each(data.votes, function (i, item) {
                        voteStr += "<p>Yes_Count:&nbsp" + item.yes_count + "&nbsp&nbsp&nbsp&nbspId:&nbsp" + item.id + "<br />"
                    });
                    //str += voteStr;

                    actionStr += "<h3>Actions: </h3><br />";
                    actionStr += "<table class='table text-center table-stripe'><tr><td>Date</td><td>Action</td><td>actor</td></tr>";
                    $.each(data.actions, function (i, item) {
                        actionStr+="<tr><td>"+item.date+"</td><td>"+item.action+"</td><td>"+item.actor+"</td></tr>"
                    });


                    str += sourceStr + voteStr+actionStr;
                    $('#fromTag').html('');
                    $('#fromTag').addClass('jumbotron');
                    $('#fromTag').html(str);
                    str = "";
                    sourceStr = "";
                    voteStr = "";
                    actionStr = "";

                    
                });
                
            })
        });
        return false;
    });
  
    
    
    $(document).click(function () { $('#fromTag').html(''); $('#fromTag').removeClass('jumbotron'); });
    
});