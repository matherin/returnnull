namespace EachVoice.Models.CustomU
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class UserComment
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(450)]
        public string blid { get; set; }

        [Required]
        [Display(Name ="Your Comment")]
        public string comt { get; set; }

        [Key]
        [Column(Order = 1)]
        public string netuid { get; set; }

        [Required]
        [Display(Name ="Bill Title")]
        public string bltitle { get; set; }
    }
}
