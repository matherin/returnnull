namespace EachVoice.Models.CustomU
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class UCContext : DbContext
    {
        public UCContext()
            : base("name=UCContext")
        {
        }

        public virtual DbSet<UserComment> UserComments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
