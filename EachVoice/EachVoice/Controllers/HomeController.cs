﻿using EachVoice.Models.CustomU;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using static EachVoice.Controllers.ManageController;

namespace EachVoice.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationUserManager _userManager;
        private ApplicationSignInManager _signInManager;
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ActionResult Index()
        {

            return View();
        }

        public ActionResult About()
        {


            return View();
        }



        [HttpGet]
        public ActionResult BillHub()
        {
            return View();
        }

        [HttpGet]
        public ActionResult BillPage(string billID, string title)
        {
            ViewBag.BillID = billID;
            ViewBag.BillTitle = title;
            return View();
        }

        //[HttpGet]
        //public JsonResult SearchByTag(string key)
        //{
        //    var data = "";
        //    var state = "";

        //    using (WebClient client = new WebClient())
        //    {
        //        state = "or";
        //        data = client.DownloadString("https://openstates.org/api/v1/bills/?state=" + state + "&subject=" + key);
        //        //Debug.WriteLine("data: " + data);
        //    }


        //    return Json(data, JsonRequestBehavior.AllowGet);
        //}

        [HttpGet]
        public ActionResult TagDetails()
        {

            return View();
        }

        //[HttpPost]
        //public ActionResult TagDetails(string key)
        //{
        //    ViewBag.Key = key;
        //    return View();
        //}

        [HttpGet]
        public ActionResult RepHub()
        {
            return View();
        }


        //This is no longer necessary. Delete after integrating in Sprint 3.
        [HttpGet]
        public ActionResult RepPage(string legID, string fullName)
        {
            ViewBag.LegID = legID;
            ViewBag.FullName = fullName;
            return View();
        }

        //DELETE THIS AT SOME POINT. IT IS NOT NEEDED.
        [HttpGet]
        public ActionResult StateSenPrime()
        {
            return View();
        }

        [Authorize]
        [HttpGet]
        public ActionResult ViewComments()
        {

            if (UserManager.FindById(User.Identity.GetUserId()).PhoneNumberConfirmed)
            {
                string id = User.Identity.GetUserId();
                using (UCContext db = new UCContext())
                {
                    var list = db.UserComments.Where(u => u.netuid.Equals(id)).ToList();
                    if (list.Count() < 1) ViewBag.Message = "You have not made any comments yet, go ahead join the community and give some of your opinion now!";
                    return View(list);
                }

            }
            else
            {
                //ViewBag.Message = "Please Verify your phone number.";
                return RedirectToAction("index", "Manage", new { Message = ManageMessageId.ConfirmPhoneNumber });
            }

        }
        [HttpGet]
        public ActionResult Edit(string blid, string uid)
        {
            if (blid == null || uid == null)
            {
                RedirectToAction("Index", "Manage");
                ViewBag.Message = "Something went wrong, try again";

            }
            using (UCContext db = new UCContext())
            {
                var com = db.UserComments.FirstOrDefault(c => c.blid == blid && c.netuid == uid);
                return View(com);
            }
            return View();
        }

        [HttpPost]
        public ActionResult Edit(string blid, string uid, UserComment comment)
        {
            UserComment com;
            using (UCContext db = new UCContext())
            {
                com = db.UserComments.FirstOrDefault(c => c.blid == blid && c.netuid == uid);
                if (com == null)
                {
                    ViewBag.Message = "Something went wrong try again later";

                    RedirectToAction("Index", "Manage");
                }
                if (TryUpdateModel(com, "", new string[] { "comt","bltitle" }))
                {
                    try
                    {
                        db.SaveChanges();
                        return RedirectToAction("Index", "Manage");
                    }
                    catch (Exception ex)
                    {

                        ModelState.AddModelError("","Edit Failed: "+ex.Message);
                    }
                }
            }
            return View(com);
        }
        //[HttpGet]
        //public ActionResult Details(string blid, string uid)
        //{
        //    using(UCContext db = new UCContext()) { if(db.UserComments.Where(c=>c.blid.Equals(blid)))}
        //    return View();
        //}
        [HttpGet]
        public ActionResult Delete(string blid, string uid)
        {
            if (blid != null && uid != null)
            {
                using (UCContext db = new UCContext())
                {
                    var c = db.UserComments.FirstOrDefault(com => com.blid.Equals(blid) && com.netuid.Equals(uid));
                    if (c == null)
                    {
                        ViewBag.Message = "can not delete anything";
                        return RedirectToAction("index", "Manage");
                    }
                    return View(c);
                }
            }
            return View();
        }

        [HttpPost]
        public ActionResult Delete(string blid, string uid, UserComment ct)
        {
            UserComment c;
            using (UCContext db = new UCContext())
            {
                c  = db.UserComments.FirstOrDefault(com => com.blid.Equals(blid) && com.netuid.Equals(uid));
                if (c != null)
                {
                    try
                    {
                        db.UserComments.Remove(c);
                        db.SaveChanges();
                        return RedirectToAction("Index", "Manage", new { Message = ManageMessageId.SucceedDeletion });
                    }
                    catch (Exception ex) { ModelState.AddModelError("", "deletion failed: " + ex.Message); }


                }

            }
            return View(c);
        }

        [Authorize]
        [HttpGet]
        public ActionResult CreateComments(string bill_id, string ttl)
        {
            var list = new List<UserComment>();
            if(bill_id==null || ttl == null) { return HttpNotFound(); }
            Debug.Write(bill_id);
            Debug.WriteLine(ttl);
            using (UCContext db = new UCContext())
            {
               list = db.UserComments.Where(c => c.blid.Equals(bill_id)).ToList();
            }
            if (list.Count() == 0) { ViewBag.Message = "Hi there is no comments yet, be the first one to leave a comment!"; }
            return View(list);
        }
        [Authorize]
        [HttpPost]
        public ActionResult CreateComments(string bill_id, string ttl, string comment)
        {

            return View();
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);

        }

        //ADDED
        //This is no longer necessary. Delete after integrating in Sprint 3.
        public ActionResult RepPage2(string fedID)
        {
            ViewBag.FedID = fedID;
            return View();
        }

        public ActionResult CalendarOfEvents()
        {
            return View();
        }
    }
}