use [EVUser]
go
drop database if exists [EVUser]	
create database [EVUser] 
on primary(name=[EVUser],
filename='C:\Users\mazhe\Documents\cs461\returnnull_zhendong\EachVoice\EachVoice\App_Data\EVUser.mdf')
log on(name=[User_log],
 filename='C:\Users\mazhe\Documents\cs461\returnnull_zhendong\EachVoice\EachVoice\App_Data\EVUser_log.ldf')

drop table [dbo].[AspNetUserClaims]
drop table [dbo].[AspNetUserLogins]
drop table [dbo].[AspNetUserRoles] 
drop table [dbo].[AspNetRoles]
drop table [dbo].[AspNetUsers]

use [EVUser]
go

CREATE TABLE [dbo].[AspNetRoles] (
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY ([Id])
)
--select * from [AspNetRoles]
--insert into [AspNetRoles] values(1,'admin');
CREATE TABLE [dbo].[AspNetUsers] (
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256),
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max),
	[SecurityStamp] [nvarchar](max),
	[PhoneNumber] [nvarchar](450),
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL default 1,
	[LockoutEndDateUtc] [datetime],
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[County] [nvarchar](100) not null,	
	CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY ([Id])
)

alter table [aspnetusers] add PhoneNumber_Converted as (case when PhoneNumber is null then cast(id as nvarchar(450)) else PhoneNumber end)
CREATE UNIQUE INDEX UQPhoneNumber_idx ON [aspnetusers] (PhoneNumber_Converted)
GO
insert into aspnetusers ([Id],[EmailConfirmed],[PhoneNumber],[PhoneNumberConfirmed],[TwoFactorEnabled],
	[LockoutEnabled],[AccessFailedCount],[UserName],[County])values('1',0,'2069460762',0,1,1,0,'abc','wa')
insert into aspnetusers ([Id],[EmailConfirmed],[PhoneNumber],[PhoneNumberConfirmed],[TwoFactorEnabled],
	[LockoutEnabled],[AccessFailedCount],[UserName],[County])values('2',0,null,0,1,1,0,'ab','wa')
insert into aspnetusers ([Id],[EmailConfirmed],[PhoneNumber],[PhoneNumberConfirmed],[TwoFactorEnabled],
	[LockoutEnabled],[AccessFailedCount],[UserName],[County])values('3',0,null,0,1,1,0,'a','wa')






CREATE TABLE [dbo].[AspNetUserClaims] (
	[Id] [int] NOT NULL IDENTITY,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max),
	[ClaimValue] [nvarchar](max),
	CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY ([Id])
)
CREATE INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]([UserId])
CREATE TABLE [dbo].[AspNetUserLogins] (
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY ([LoginProvider], [ProviderKey], [UserId])
)
CREATE INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]([UserId])
CREATE TABLE [dbo].[AspNetUserRoles] (
	[RoleId] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY ([RoleId], [UserId])
)
CREATE INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]([RoleId])
CREATE INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]([UserId])
ALTER TABLE [dbo].[AspNetUserClaims] ADD CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[AspNetUserLogins] ADD CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[AspNetUserRoles] ADD CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY ([RoleId]) REFERENCES [dbo].[AspNetRoles] ([Id]) ON DELETE CASCADE
ALTER TABLE [dbo].[AspNetUserRoles] ADD CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE

create table [dbo].[UserComments]
(
	--id int not null identity(1,1),
	blid nvarchar(450) not null,
	comt nvarchar(max) not null,
	netuid nvarchar(128),
	constraint [pk_UserComments] primary key ([blid],[netuid])
)

use [EVUser]
go
select * from [AspNetUsers]
select * from [AspNetUserLogins]
select * from [AspNetUserRoles]
select * from [AspNetRoles]
delete from [aspnetusers]
select * from usercomments	
update usercomments set netuid = 'e03a9087-ccdf-4b5a-b72c-bb026fd0cb1e' where comt='well done'
select * from aspnetuserclaims
alter table usercomments  add bltitle nvarchar(max) not null default 'nothing'
insert into usercomments (blid,comt,netuid) values('WAB00012057',
		'it is good to have this bill passed so that we can save the earth',
		'6e9 8077d-3df9-48ff-ae9d-c21d95a79377');
insert into usercomments (blid,comt,netuid) values('WAB00012057',
		'x + y = xy?',
		'6e9 8077d-3df9-48ff-ae9d-c21d95a79377');
insert into usercomments (blid,comt,netuid) values('WAB00012057',
		'hahaha it is useful site that we spill out our own opinion',
		'6e9 8077d-3df9-48ff-ae9d-c21d95a79377');